# XYZ GOAT React.js Code Challenge Solution

This Todo List app is my solution for [XYZ GOAT React.js Code Challenge](https://github.com/the-goat-jp/react-challenge)

## Requirement

Make sure node already installed. I'm using node version 16

```bash
# using NVM
nvm install 16
nvm use 16

node -v
# v16.20.2
```

## Commands

In the project directory, you can run:

### `yarn`

Before running the app, run this command to install all the dependencies.


### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

Please make sure to run `yarn server` too at the same time, to run the API server.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn server`

Run the API server with JSON Server.
