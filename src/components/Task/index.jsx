import PropTypes from 'prop-types'

import useDeleteTodo from '../../hooks/useDeleteTodo'

/**
 * Render individual task/todo item as a list item for TodoList component
 *
 * @param {Object} props The component props
 * @param {number} props.id Id of the task
 * @param {string} props.title Title of the task
 *
 * @returns {JSX.Element} React element
 */
export default function Task(props) {
  const { id, title } = props
  const { deleteTodo, status, error } = useDeleteTodo(id)

  function handleDelete() {
    deleteTodo()
  }

  return (
    <li className="p-2 rounded-lg" data-testid="todo-list-item">
      <div className="flex align-middle flex-row justify-between flex-wrap">
        <div className="p-2">
          <p className="text-lg text-black">{title}</p>
        </div>
        <button
          type="button"
          onClick={handleDelete}
          disabled={status === 'loading'}
          data-testid="todo-list-item-delete-btn"
          className="flex text-red-500 border-2 border-red-500 p-2 rounded-lg"
        >
          <svg
            className="h-6 w-6 text-red-500"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <circle cx="12" cy="12" r="10" />{' '}
            <line x1="15" y1="9" x2="9" y2="15" />{' '}
            <line x1="9" y1="9" x2="15" y2="15" />
          </svg>
          <span> {status === 'loading' ? 'Deleting...' : 'Delete'}</span>
        </button>
      </div>
      <hr className="mt-2" />

      {status === 'error' && (
        <div className="text-red-500 text-xs italic">
          Failed to delete: {error?.message}
        </div>
      )}
    </li>
  )
}

Task.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
}
