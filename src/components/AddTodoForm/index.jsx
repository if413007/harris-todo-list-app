import { useState } from 'react'

import useAddTodo from '../../hooks/useAddTodo'

/**
 * Form that handle adding new task/todo
 *
 * @returns {JSX.Element} React element
 */
export default function AddTodoForm() {
  const [newTodo, setNewTodo] = useState('')
  const { addTodo, status, error } = useAddTodo({
    onSuccess: () => {
      // Clear the input field so user can easil add more items
      setNewTodo('')
    },
  })

  /**
   * @param {React.FormEvent<HTMLInputElement>} event
   */
  function handleChangeInput(event) {
    setNewTodo(event.target.value)
  }

  /**
   * @param {React.FormEvent<HTMLFormElement>} event
   */
  function handleSubmit(event) {
    event.preventDefault()
    addTodo(newTodo)
  }

  return (
    <form onSubmit={handleSubmit} className="mb-4">
      <div className="mt-4 flex">
        <input
          type="text"
          data-testid="new-task-input"
          value={newTodo}
          onChange={handleChangeInput}
          className="w-80 border-b-2 border-gray-500 text-black p-2"
          disabled={status === 'loading'}
          placeholder="New task.."
        />

        <button
          type="submit"
          disabled={status === 'loading'}
          data-testid="addtodoform-submit-btn"
          className="ml-2 border-2 border-green-500 p-2 text-green-500 hover:text-white hover:bg-green-500 rounded-lg flex"
        >
          <svg
            className="h-6 w-6"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            strokeWidth="2"
            stroke="currentColor"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
          >
            <path stroke="none" d="M0 0h24v24H0z" />{' '}
            <circle cx="12" cy="12" r="9" />{' '}
            <line x1="9" y1="12" x2="15" y2="12" />{' '}
            <line x1="12" y1="9" x2="12" y2="15" />
          </svg>
          <span>{status === 'loading' ? 'Adding...' : 'Add'}</span>
        </button>
      </div>

      {status === 'error' && (
        <div
          className="text-red-500 text-xs italic"
          data-testid="addtodoform-error-msg"
        >
          {error?.message}
        </div>
      )}
    </form>
  )
}
