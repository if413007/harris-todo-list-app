import AddTodoForm from '../AddTodoForm'
import Task from '../Task'
import useTodoList from '../../hooks/useTodoList'

/**
 * Todo List
 *
 * Main component that shows todo list and form to add new one
 *
 * @returns {JSX.Element} React element
 */
export default function TodoList() {
  const { isLoading, isError, todoList, error } = useTodoList()

  const hasList = todoList !== null && todoList !== undefined
  const isEmptyList = hasList && todoList.length <= 0

  return (
    <div>
      <h1 className="text-3xl font-bold text-center underline">TODO LIST</h1>

      <AddTodoForm />

      {isLoading && <span>Loading...</span>}
      {isError && (
        <>
          <span>
            Failed to refresh the list:{' '}
            {error?.message || 'Failed to connect to the server'}
          </span>
          <p className="italic">
            {'(make sure the API server already running)'}
          </p>
        </>
      )}

      {isEmptyList && (
        <div className="text-center italic">
          No todo item found, please add one!
        </div>
      )}

      {!isEmptyList && (
        <ul>
          {todoList?.map(({ id, title }) => (
            <Task key={id} id={id} title={title} />
          ))}
        </ul>
      )}
    </div>
  )
}
