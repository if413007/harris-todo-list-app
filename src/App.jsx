import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import TodoList from './components/TodoList'

const queryClient = new QueryClient()

export default function App() {
  return (
    <div className="w-full py-8">
      <div className="bg-white p-3 max-w-md mx-auto rounded-md shadow-lg">
        <QueryClientProvider client={queryClient}>
          <TodoList />
        </QueryClientProvider>
      </div>
    </div>
  )
}
